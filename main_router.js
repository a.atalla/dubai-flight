const Router = require('koa-router')
const MongoClient = require('mongodb').MongoClient

const router = new Router()
const {parse} = require('./parser')

const BASE_URL = '/api/v1'
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/flight_app'

router.post(`${BASE_URL}/search`, async (ctx) => {
  try {
    let txt = ctx.request.body.q
    let q = await parse(txt) // will convert the search string to an object
    // { flight: undefined,
          // cities: { collection: 'arrivals', originName: 'Heathrow' },
          // dayName: undefined }
    // will search the database based on the above q object
    
    let query = {
      '$or':[
        {'lang.en.originName': q.city.cityName},
        {'lang.en.destinationName': q.city.cityName}
      ]}

    if(q.flight){
      query['fullName'] = q.flight
    }

    if (q.date) {
      // generate range of unix timestampms from the day date  
      // q.date is the date calculated by dayname(sunday, monday, ...etc) or words (today, weekend)
      // will qury for    {schedule $gte: startDate, $lt: endDate}

      startDate =  new Date(q.date.getFullYear(), q.date.getMonth(), q.date.getDate()).getTime()/1000|0
      endDate = new Date(q.date.getFullYear(), q.date.getMonth(), q.date.getDate()+1).getTime()/1000|0
      query['scheduled'] = {
        '$gte': startDate,
        '$lt': endDate
      }
    }
    let db = await MongoClient.connect(MONGODB_URI) 
    // const db = client.db('flight_app')
    let result
    result = await db.collection(q.city.collection).find(query).toArray()
    db.close()
    // response
    ctx.body= result
  } catch (err) {
    console.log(err)
    ctx.body= {
      'error': err
    }
  }
})

module.exports = router
