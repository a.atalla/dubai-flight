const MongoClient = require('mongodb').MongoClient
const _ = require('lodash')
// const moment = require('moment')

const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/flight_app'

// match a text from an array with regex
const findInArray = ( arr, str ) => {
  return str.match(new RegExp( arr.join("|"), "gi" ) )
}


// parse for flight name (company)
const parseFlight = async (searchText) => {
  try{
    let db = await MongoClient.connect(MONGODB_URI) 
    let flights = await db.collection('flights').find({}, {_id: false, airlineName: true}).toArray()
    db.close()
    
    // get an array of flights name from the db result
    flights = flights.map((a) => {
      return a.airlineName.toLowerCase().trim() 
    }).sort()

    let flightName

    flightName = findInArray(flights, searchText)

    return _.startCase(_.camelCase(flightName))
  } catch(err){
    console.log(err)
  }
}
// search for cities new
const parseCities = async (searchText)=> {
  try{
    const searchWords = searchText.toLowerCase().split(' ')  
    let db = await MongoClient.connect(MONGODB_URI) 
    let arrivalsFrom = await db.collection('arrivals').aggregate([{
      $project: {
        _id: false,
        city: '$lang.en.originName'
      }
    }]).toArray()
    let departureTo = await db.collection('departures').aggregate([{
      $project: {
        _id: false,
        city: '$lang.en.destinationName'
      }
    }]).toArray()
    db.close()
  
    // get array of city names removing duplicates
    arrivalsFrom = _.uniqBy(arrivalsFrom, 'city').map((city) => {
      return city.city.toLowerCase().trim() 
    }).sort()
  
    // get array of city names  removing duplicates
    departureTo = _.uniqBy(departureTo, 'city').map((city) => {
      return city.city.toLowerCase().trim() 
    }).sort()
  
    let allCities = _.union(arrivalsFrom, departureTo)
  
    let citiesInText = findInArray(allCities, searchText)  // array of cities from searchText or null
  
    if (citiesInText){
      let cityName = citiesInText[0]
      let testWord = searchWords[searchWords.indexOf(cityName.split(' ')[0]) - 1 ] // split the city name in case of multiple word city 
      if (testWord === 'to') {
        return {
          collection: 'departures',
          cityName: _.startCase(_.camelCase(cityName))
        }
      } else if(testWord === 'from') {
        return {
          collection: 'arrivals',
          cityName: _.startCase(_.camelCase(cityName))
        }
      } else {
        // if there is no "to" or "from" default to "departures from dubai"
        return {
          collection: 'departures',
          cityName: 'Dubai'
        }
      }
    }
  } catch(err){
    console.log(err)
  }
 
}

const parseDayname = (searchText, today) => {
  let searchWords = searchText.toLowerCase().split(' ')
  const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
  let dayName  
  searchWords.forEach((word) => {
    if (days.indexOf(word) > -1){
      dayName = word
    } else if (word === 'weekend') {
      dayName = 'friday'
    } else if (word === 'today'){
      dayName = days[today.getDay()]
    }
  })

  if(dayName){
    let deltaDays = days.indexOf(dayName) - today.getDay()
    let theDate = today
    
    if (deltaDays > 0){
      theDate = theDate.setDate(theDate.getDate() + deltaDays)
    } else if (deltaDays < 0) {
      theDate = theDate.setDate(theDate.getDate() + (7+deltaDays))
    } else {
      theDate.setDate(theDate.getDate())
    }
    return new Date(theDate)
    }
}

// main function
const parse = async (searchText)=> {
  let q = {}
  q['flight'] = await parseFlight(searchText)
  q['city'] = await parseCities(searchText)
  q['date'] = parseDayname(searchText, new Date())
  
  return q
}

module.exports = {parse, parseFlight, parseCities, parseDayname}

