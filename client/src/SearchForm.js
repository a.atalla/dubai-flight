import React from 'react'
import PropTypes from 'prop-types'

const styles = {
  form:{
    display: 'flex',
    justifyCOntent: 'center',
    margin: '0p',
    padding: '0',
    width: '100%'
  },
  input: {
    margin: 'auto',
    width: '90%',
    backgroundColor: '#FFF',
    border: 'none',
    outline: 'none',
    fontSize: '1.5em',
    color: '#A1A9B4'
  },
  inputWithIcon: {
    width: '90%',
    margin: 'auto',
    display: 'flex',
    position: 'relative',
    padding:'10px 10px',
  },
  icon: {
    color: '#B1B8C1',
    margin: 'auto',
    position: 'absolute',
    right: '10px',
    top: '12px',
    fontSize: '2em',
  }
}
const SearchForm  = ({handleSubmit}) => {

  return (
    <form onSubmit={handleSubmit} style={styles.form} method="POST">
      <div style={styles.inputWithIcon}>
        <input type="text" name="searchText" style={styles.input}/>
        <i className="fa fa-search" style={styles.icon}></i>
      </div>
    </form>
  )

}

SearchForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired
}
export default SearchForm