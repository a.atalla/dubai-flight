import React, { Component } from 'react'
import PropTypes from 'prop-types'
import SingleFlight from './SingleFlight'

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around'
  }
}
class FLightsList extends Component{
  
  renderList = () => {
    return this.props.flights.map((f) => {
      return (
        <SingleFlight key={f.uid} flight={f}/>
      )
    })
  }

  render = () => {
    return (
      <div style={styles.container}>
        {this.renderList()}
      </div>
    )
  }
}

FLightsList.propTypes = {
  flights: PropTypes.array.isRequired
}

export default FLightsList