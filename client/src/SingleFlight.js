import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
const styles = {
  container: {
    marginTop: '15px',
    display: 'flex',
    justifyContent: 'space-between',
    paddingBottom: '5px',
    borderBottom: '1px solid #eee'
  },
  icon: {
    marginRight: '25px',
    color: '#47C391',
    fontSize: '1.4em',
    fontWeight: '100'
  },
  strong: {
    margin: '0 5px 0 5px',
    color: '#959EAA',
    fontWeight: 'bold'
  },
  light: { 
    margin: '0 5px 0 5px',
    color: '#CACED5'
  },
  time:{  
    color: '#47C391'
  }

}
const SingleFLight = (props) => {
    let sched = props.flight.scheduled
    return (
      <div style={styles.container}>
        <i className="fa fa-plane" style={styles.icon}></i>
        <span style={styles.strong}>{props.flight.fullName} </span>
        <span style={styles.light}>{props.flight.flightNumber}</span>
        <span style={styles.strong}>{props.flight.destinationName}</span>
        <span style={styles.light}>at</span>
        <span style={styles.time}>{moment(sched*1000).format('LLLL')}</span>  {/* multiply by 1000  to convert unix time to ms */}
      </div>
    )
}

SingleFLight.propTypes = {
  flight: PropTypes.object.isRequired
}

export default SingleFLight