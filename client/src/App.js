import React, { Component } from 'react'
import axios from 'axios'
import Spinner from 'react-spinkit'
import SearchForm from './SearchForm'
import FlightsList from './FlightsList'

const styles = {
  container:{
    // fontFamily: 'Roboto',
    background: '#FFF',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 'auto',
    marginBottom: '50px',
    padding: '25px',
    width: '40%',
    border: '1px solid #ccc',
    borderTopRightRadius: '25px',
    borderTopLeftRadius: '25px'
  },
  title: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '200px  auto 20px auto',
    padding: '25px',
    width: '40%',
    fontSize: '3em',
    fontWeight: '600',
    color: '#47C391'
  },
  subtitle: {
    color: '#B1B8C1',
    fontSize: '2em'
  }
}

class App extends Component{
  constructor (props) {
    super(props)
    this.state = {
      result: [],
      isLoading: false
    }
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.setState({
      isLoading: true
    })
    axios.post('/api/v1/search', {q: e.target.searchText.value})
      .then((result) => {
        this.setState({
          result: result.data,
          isLoading: false
        })
      })
      .catch((err) => {
        console.log(err)
      })
    }

  renderList = () => {
    if (this.state.isLoading){
      return <Spinner name="circle" />
    } else {
      if (this.state.result.length > 0){
        return <FlightsList flights={this.state.result} />
      } else {
        return <h4 style={styles.subtitle}>No Flight Found</h4> 
      }
    }
  }

  render = () => {
    return (
      <div>
        <h1 style={styles.title}>Dubai Flight Finder</h1>
        <div style={styles.container}>
          <SearchForm handleSubmit={this.handleSubmit}/>
          { this.renderList() }
          
        </div>
      </div>
      
    )
  }
}

export default App