const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const serve = require('koa-static')

const app = module.exports = new Koa()

const scheduler = require('./scheduler')
const mainRouter = require('./main_router')

const PORT = process.env.PORT || 3030

if (process.env.NODE_ENV === 'test') {
  PORT = 3333
}

app.use(bodyParser())
app.use(mainRouter.routes())

// Serve react build directory
app.use(serve('./client/build'))

const server = app.listen(PORT, () => {
  console.log(`Listing to http://127.0.0.1:${PORT}`)
  scheduler()
})

module.exports = server
