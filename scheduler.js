// Reference the node-schedule npm package.
const schedule = require('node-schedule');
const MongoClient = require('mongodb').MongoClient
const axios = require('axios')
// const Promise = require("bluebird")

// GET http://www.dubaiairports.ae/api/flight
// GET http://www.dubaiairports.ae/api/flight/arrivals
// GET http://www.dubaiairports.ae/api/flight/departures
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/flight_app'

module.exports = function() {

  MongoClient.connect(MONGODB_URI, (err, db) => {
    if (err){
      console.log(err)
    } else {
      db.collection('flights').ensureIndex('airlineName')
      db.collection('arrivals').ensureIndex('flightNumber')
      db.collection('departures').ensureIndex('flightNumber')

      db.collection('flights').find().count()
        .then((flightsCount) => {
          if (flightsCount === 0){
            return axios.get('http://www.dubaiairports.ae/api/flight')
          } else {
            throw('Data Already exists')
          }
        })
        .then((result) => {
          return db.collection('flights').insertMany(result.data)
        })
        .then(() => {
          db.close()
        }) 
        .catch((err) => {
          console.log(err)
        })
    }
  })    

  let getArrivals = schedule.scheduleJob("* * * * *", async function() {
    try{
      let db = await MongoClient.connect(MONGODB_URI) 
      // const db = client.db('flight_app')
      let result = await axios.get('http://www.dubaiairports.ae/api/flight/arrivals')  
      await db.collection('arrivals').deleteMany({}) 
      await db.collection('arrivals').insertMany(result.data.flights)
      db.close()
    } catch (err){
      console.log(err)
    }
  })

  let getDepartures = schedule.scheduleJob("* * * * *", async function() {
    try{
      let db = await MongoClient.connect(MONGODB_URI) 
      // const db = client.db('flight_app')
      let result = await axios.get('http://www.dubaiairports.ae/api/flight/departures')   
      await db.collection('departures').deleteMany({}) 
      await db.collection('departures').insertMany(result.data.flights)
      db.close()
    } catch (err){
      console.log(err)
    }
  })

}
