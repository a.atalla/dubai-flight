process.env.NODE_ENV = 'test'
const chai = require('chai')
const should = chai.should()

const {parseFlight, parseCities, parseDayname} = require('../parser')

describe('Parser', ()=> {

  describe('day name and date', () => {
    it('should return date from a day name (1 day before)', async () => {
      const startOfTime = new Date(0) // thursday(4) 1970-01-01T00:00:00.000Z
      let searchText = 'wednesday'
      let theDate = await parseDayname(searchText, startOfTime)
      theDate.getDay().should.equal(3)
      theDate.getDate().should.equal(7)  // should be 07-01-1970
    })
  
    it('should return date from a day name (same day)', async () => {
      const startOfTime = new Date(0) // thursday(4) 1970-01-01T00:00:00.000Z
      let searchText = 'thursday'
      let theDate = await parseDayname(searchText, startOfTime)
      theDate.getDay().should.equal(4)
      theDate.getDate().should.equal(1)  // should be 01-01-1970
      
    })
  
    it('should return date from a day name (1 day after)', async () => {
      const startOfTime = new Date(0) // thursday(4) 1970-01-01T00:00:00.000Z
      let searchText = 'friday'
      let theDate = await parseDayname(searchText, startOfTime)
      theDate.getDay().should.equal(5)
      theDate.getDate().should.equal(2)  // should be 02-01-1970
      
    })
  })

  describe('flight carrier name', () => {
    it('should return carrier name (single word)', async () => {
      let searchText = 'emirates flight to cairo'
      let flightName = await parseFlight(searchText)
      flightName.should.equal('Emirates')
    })

    it('should return carrier name (multiple words)', async () => {
      let searchText = 'Pakistan International Airlines flight from dubai to islamabad'
      let flightName = await parseFlight(searchText)
      flightName.should.equal('Pakistan International Airlines')
    } )
  })

  describe('find city name', () => {
    it('should return city name (from)', async () => {
      let searchText = ('travel to dubai from cairo')
      let city = await parseCities(searchText)
      should.exist(city.collection)
      should.exist(city.cityName)
      city.collection.should.equal('arrivals')
      city.cityName.should.equal('Cairo')  // extract the city and return it capitalized
    })

    it('should return city name (to multiple word city)', async () => {
      let searchText = ('travel from dubai to addis ababa')
      let city = await parseCities(searchText)
      should.exist(city.collection)
      should.exist(city.cityName)
      city.collection.should.equal('departures')
      city.cityName.should.equal('Addis Ababa')  // extract the city and return it capitalized
    })
  })

  
})
