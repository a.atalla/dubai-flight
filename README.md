## How to run
- This project use Koa2 and async/await technique so you need `node.js >= 7.6` or use a transpiler 
- `mongodb` should be install and the server is running on the default `port 27017`
- clone the repo locally  `git clone https://gitlab.com/a.atalla/dubai-flight.git`
- change directory to `dubai-flight` the install the dependencies

```bash
# install server dependencies
cd dubai-flight
npm install 


# install client dependencies
cd client
npm install

# run for development (client will run on port 3000 )
npm start 

# OR

# build for production (client will be served from client/build via koa-static on the server)
npm run build


#start the server (server will run on port 3030)
cd ..
npm start 
```
